<?php

namespace Moresby\Basic;

class Colors {

    private string $source;
    public array $list = [];

    /**
     * @param string $source - the source URL from which to retrieve a list of colors
     */
    public function __construct(string $source = "")
    {
        /**
         * Don't bother setting the source prop
         * if we haven't passed a valid url
         */
        if(filter_var($source,FILTER_VALIDATE_URL)) {
            $this->source = $source;
        }

    }

    /**
     * Retrieve a list of colors from a given source
     * @return json array
     */
    public function getList() : array
    {
        if(!empty($this->source)) {
            if($json = json_decode(file_get_contents($this->source),true)) $this->list = $json;
        }

        return $this->list;

    }

    /**
     * Generate an element used to display the color to the user
     * @param string color - the name of a color to be used as a class
     * @return string - fully-formed html element
     */
    public function buildElement(string $color)
    {
        return "<div class='colorItem' style='background-color:${color}'></div>";
    }



}