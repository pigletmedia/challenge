<?php
/**
 * Bootstrap composer autoloading
 */
require 'vendor/autoload.php';

use Moresby\Basic\Colors;

$listSource = "https://rw-jackfruit-public-dev.s3.us-west-2.amazonaws.com/colors.json";

$Colors = new Colors($listSource);
$colorList = $Colors->getList();

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="assets/main.css">
    <title>Moresby - Basic</title>
</head>
<body>
<div id="colorsWrapper">
    <div id="colorsGrid">
        <?php
        
        /**
         * Display each color in the retrieved list
         * as pre-formatted element
         */
        foreach($colorList as $color) {
            echo $Colors->buildElement($color);
        }

        ?>
    </div>
</div>
</body>
</html>