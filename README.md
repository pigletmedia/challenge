# Timeline

Start Time: 6:38pm
Analyze/Architect/Design: 6:50pm
Proof Of Concept: 7:05pm
Complete: 8:03pm
Advanced Version: 8:35pm

# Refactoring

When completing this challenge, I found myself exploring some new technologies
like Vuetify and Mocha. During this process I refactored some of the code
when appropriate - these experiments are not recorded in the above timeline
